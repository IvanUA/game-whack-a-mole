'use strict';
// будь-яка непідсвічена комірка в таблиці на короткий час забарвлюється в синій колір

const cellsHTMLCollection = document.getElementsByClassName("table__cell");
const arrayFromCells = [...cellsHTMLCollection]; // forEach didn`t work with HTMLCollection
const tryAgainBtn = document.querySelector(".try-again");
const compWin = document.querySelector(".comp__win");
const playerWin = document.querySelector(".player__win");
const button = document.querySelector(".button_start"); 
const levelsContainer = document.querySelector(".levels__container");
const table = document.querySelector(".table");
const formLevels = document.querySelector(".form_levels")
const levelSelect = document.getElementById('level');
let computerScore = 0;
let playerScore = 0;
let intervalId;
let intervalTime;

function randomCellturnBlue() {
    const randomIndex = Math.floor(Math.random() * arrayFromCells.length);
    arrayFromCells[randomIndex].style.background = 'blue';
    arrayFromCells.forEach(cell => {
        // if (cell.style.background === 'green') {

        // }
        setTimeout(() => {
            if (cell.style.background === 'blue') {
                cell.style.background = 'red';
            }
        }, 1400)
    })
    scoreChecker();
}

function startInteval() {
    chooseLevel();
    intervalId = setInterval(randomCellturnBlue, intervalTime);
}

function playerCollorChanger() {
    arrayFromCells.forEach(cell => {
        cell.addEventListener('click', () => {
            if (cell.style.background === 'blue') {
                cell.style.background = 'green';
            }
        })
    });
}

playerCollorChanger()

function pauseStart() {
     button.addEventListener("click", () => {
        if (button.innerHTML === 'Start') {
            startInteval(intervalId);
            button.innerHTML = 'Pause';
            button.style.background = 'yellow'
            levelsContainer.style.display = "none";
            
        } else {
            clearInterval(intervalId);
            button.innerHTML = 'Start';
            button.style.background = ''
            levelsContainer.style.display = "flex";
        }
    })
}
pauseStart();

function scoreChecker() {
    const playerScoreDesk = document.querySelector(".counter__player");
    const computerScoreDesk = document.querySelector(".counter__computer");
    arrayFromCells.forEach(item => {
        if (item.style.background === 'green') {
             playerScore += 1;
            playerScoreDesk.innerHTML = `Player points: ${playerScore}  / 566`
        }
        if (item.style.background === 'red') {
             computerScore += 1;
            computerScoreDesk.innerHTML = `Computer points: ${computerScore}  / 566`
        }
    })
    // console.log(`Player ${playerScore}`);
    // console.log(`Computer ${computerScore}`);
    checkWinner()
}

scoreChecker();

function checkWinner() {

    if (playerScore >= 566) {
        console.log("player won");
        playerWin.style.display = "flex";
        tryAgainBtn.style.display = "block";
        button.style.display = "none";
        levelsContainer.style.display = "none";
        table.style.display = "none";

        clearInterval(intervalId);
    }
    if (computerScore >= 566) {
        console.log("comp won");
        compWin.style.display = "flex";
        tryAgainBtn.style.display = "block";
        button.style.display = "none";
        levelsContainer.style.display = "none";
        table.style.display = "none";
        clearInterval(intervalId);
    }
}

tryAgainBtn.addEventListener('click', () => {
    location.reload();
})

function chooseLevel() {
    const selectedLevel = levelSelect.value;
    
    switch (selectedLevel) {
        case 'low':
            intervalTime = 1500;
            break;
        case 'medium':
            intervalTime = 1000;
            break;
        case 'high':
            intervalTime = 500;
            break;
        default:
            intervalTime = 1500;
    }
    console.log(selectedLevel);
}
chooseLevel();
// chooseLevel()
// checkWinner()
// 546